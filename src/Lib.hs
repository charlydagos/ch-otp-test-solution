{-# LANGUAGE BangPatterns       #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE OverloadedStrings  #-}

module Lib where


import           Control.Concurrent.MVar
import           Data.Binary             (Binary)
import           Data.List.Ordered
import           Data.Monoid             ((<>))
import           Data.Time.Exts.Unix
import           Data.Typeable           (Typeable)
import           Data.Yaml               (FromJSON (..), (.:))
import qualified Data.Yaml               as Y
import           GHC.Generics            (Generic)
import           System.Random


--------------------------------------------------------------------------------
-- Keeps information of a node
data NodeInfo = NodeInfo { hostname :: !String
                         , port     :: !Int
                         }
                         deriving Show


--------------------------------------------------------------------------------
-- Configuration file
data Config = Config { sendPeriod  :: !Int
                     , gracePeriod :: !Int
                     , peerFind    :: !Int
                     , nodes       :: ![NodeInfo]
                     }
                     deriving Show


--------------------------------------------------------------------------------
-- Interpret the config file
instance FromJSON Config where
  parseJSON (Y.Object v) =
    Config <$>
      v .: "sendPeriod"  <*>
      v .: "gracePeriod" <*>
      v .: "peerFind"    <*>
      v .: "nodes"
  parseJSON _ = fail "Expected Object for Config value"


--------------------------------------------------------------------------------
-- Interpret a NodeInfo
instance FromJSON NodeInfo where
  parseJSON (Y.Object v) =
    NodeInfo <$>
      v .: "hostname" <*>
      v .: "port"
  parseJSON _ = fail "Expected Object for Config value"


--------------------------------------------------------------------------------
-- Message that will be passed around by each node
data ProcMessage = ProcMessage UnixDateTimeMillis Double String
                   deriving (Generic, Typeable, Show, Eq)


--------------------------------------------------------------------------------
-- Messages order is defined by the associated timestamp
instance Ord ProcMessage where
  compare (ProcMessage n _ _) (ProcMessage m _ _) = compare n m


--------------------------------------------------------------------------------
-- Instances for serialization
instance Binary UnixDateTimeMillis
instance Binary ProcMessage


--------------------------------------------------------------------------------
-- Get an empty list of messages
emptyMessageList :: IO (MVar [ProcMessage])
emptyMessageList = newMVar []


--------------------------------------------------------------------------------
-- Add a message to a list oredered by sent timestamp. We don't want to lose
-- any information so for messages that have the same timestamp, order by
-- first ocurrence
addMessage :: MVar [ProcMessage] -> ProcMessage -> IO ()
addMessage messagesRef msg =
  modifyMVar_ messagesRef $ return <$> insertSetBy ordBySent msg
  where
    ordBySent x@ProcMessage {} y@ProcMessage {}
      | x > y     = GT
      | otherwise = LT


--------------------------------------------------------------------------------
-- Get the sum of all messages multiplied by the their index
getMessagesProducts :: [ProcMessage] -> Double
getMessagesProducts messages = sum $ map f zippedMessages
  where
    f (ProcMessage _ msg _, i) = i * msg
    zippedMessages = zip messages [1..]


--------------------------------------------------------------------------------
-- Get a deterministic number generator
rndGen :: Int -> [Double]
rndGen int = randoms $ mkStdGen int


--------------------------------------------------------------------------------
-- Generate a deterministic list of ProcMessage for a certain node
generateMessages :: Int -> String -> String -> [IO ProcMessage]
generateMessages !i h p = map (makeMessage h p) (rndGen i)


--------------------------------------------------------------------------------
-- Make a message
makeMessage :: String -> String -> Double -> IO ProcMessage
makeMessage h p d = do
  time <- getCurrentUnixDateTimeMillis
  return $ ProcMessage time d (h <> ":" <> p)

