module Main where

import           Control.Concurrent               (threadDelay)
import           Control.Concurrent.MVar
import           Control.Distributed.Process
import           Control.Distributed.Process.Backend.SimpleLocalnet
import           Control.Distributed.Process.Node (LocalNode, forkProcess,
                                                   initRemoteTable, runProcess)
import           Control.Monad                    (forM_, forever)
import           Control.Monad.Reader             (ReaderT, ask, lift,
                                                   runReaderT)
import qualified Data.ByteString.Char8            as B
import           Data.List.Ordered                (member)
import           Data.Maybe                       (fromMaybe)
import           Data.Monoid                      ((<>))
import qualified Data.Yaml                        as Y
import           Lib
import           Network.URI                      (isIPv4address, isIPv6address)
import           System.Console.GetOpt
import           System.Environment               (getArgs)
import           System.Exit                      as E
import           System.IO


--------------------------------------------------------------------------------
-- Flags available for the application
data Flag
    = WaitFor String     -- ^ --wait-for <int>
    | SendFor String     -- ^ --send-for <int>
    | WithSeed String    -- ^ --with-seed <int>
    deriving (Show, Eq)


--------------------------------------------------------------------------------
-- Flags with their corresponding description
flags :: [OptDescr Flag]
flags = [ Option ['p'] ["send-for"]              (ReqArg SendFor "INT")
               "Send for a specific amount of seconds"
        , Option ['w'] ["wait-for"]              (ReqArg WaitFor "INT")
               "Wait for a specific amount of seconds"
        , Option ['s'] ["with-seed"]              (ReqArg WithSeed "INT")
               "Start the node with a specific seed"
        ]


--------------------------------------------------------------------------------
-- Simple parsing of flags
parseFlags :: [String] -> Either [String] (String, String, [Flag])
parseFlags (h:p:argv)
  = case getOpt Permute flags argv of
      (args, _, []) -> Right (h, p, args)
      (_, _, errs)  -> Left errs
parseFlags _ = Left ["Invalid parameters"]


--------------------------------------------------------------------------------
-- Since there's a config to be passed around, it's in our best interest
-- to keep it along the main monads we'll be using
type ConfigIO      = ReaderT Config IO
type ConfigProcess = ReaderT Config Process


--------------------------------------------------------------------------------
-- Helper function to log to stderr
doLog :: String -> String -> String -> IO ()
doLog h p message = hPutStrLn stderr message'
                    where
                        message' = "[" <> h <> ":" <> p <> "] " <> message


--------------------------------------------------------------------------------
-- Listener process, keeps a config statically
-- It takes a mutable variable of `[ProcMessage]`, which is the main message
-- send by other nodes, and adds it ordered by the `sent` timestamp
listener :: MVar [ProcMessage] -> ConfigProcess ()
listener messages = ask >>= \config -> lift $ do
  say "Listener waiting until grace period"
  liftIO $ threadDelay (sendPeriod config)

  say "Start listening"

  forever $ do
    msg' <- expect :: Process ProcMessage
    liftIO $ addMessage messages msg'


--------------------------------------------------------------------------------
-- Sender process takes a host:port combination, along with the node descriptor
-- and a backend reference. It pushes messages for the configured amount of time
-- and is then ended forcefully. The messages are defined as an infinite list
-- of random `Double`
sender :: Int -> String -> String -> LocalNode -> Backend -> ConfigProcess ()
sender rndSeed h p node backend = do
  config <- ask
  self   <- lift getSelfPid

  lift $ say "Start sending"

  -- Keep the pid reference to kill it later
  senderPid <- liftIO $ forkProcess node $ do
    -- Send messages indefinitely
    let messages = generateMessages rndSeed h p
    forM_ messages $ \msg -> do
      -- FIXME: This is where I had issues, and what makes the solution
      -- not work 100%. I was expecting the `findPeers` function to return
      -- the peers connected, but when a peer dies this function keeps
      -- returning it as active. Ideally I'd stop sending messages until
      -- it came back, or buffer the messages to that peer for some time,
      -- or retry it, etc. But I was unable to do any of those things.
      peers <- liftIO $ findPeers backend (peerFind config)
      say $ "Found peers: " <> show peers

      -- Push the produced message to each peer
      forM_ peers $ \peer -> do
        -- Get the message with the produced (`sent`) timestamp
        msg' <- liftIO msg
        say $ "Sending message: " <> show msg' <> " to peer: " <> show peer
        -- Allow us to reconnect, just in case we were disconnected
        reconnect self
        nsendRemote peer "receive-server" msg'

  liftIO $ threadDelay (gracePeriod config)
  lift $ say "Stop sending"
  lift $ exit senderPid "Send period is over"


--------------------------------------------------------------------------------
-- This is an important function for the program. It waits until all nodes
-- from the configuration files are online. Otherwise it keeps waiting
waitForPeers :: LocalNode -> String -> String -> Backend -> ConfigIO [NodeId]
waitForPeers node h p backend = do
  config <- ask

  lift $ doLog h p "Waiting for all nodes to come online"
  peers <- lift $ findPeers backend (peerFind config)

  if allPeersOnline peers config
     then return peers
     else do
       lift $ threadDelay (peerFind config)
       waitForPeers node h p backend
  where
    -- Checks that all peers found by `findPeers` are online, according
    -- to the config file
    allPeersOnline :: [NodeId] -> Config -> Bool
    allPeersOnline peers config = all (inPeers peers) (nodes config)
    -- This function is quite dirty. I didn't find a way to recreate
    -- the nodes in config file to make them directly comparable to the `NodeId`
    -- type, but it's Good Enough (TM) for the solution
    inPeers :: [NodeId] -> NodeInfo -> Bool
    inPeers peers nodeInfo = let
                               strPeerAddrs = map (show . nodeAddress) peers
                               nodeInfoStr  = toComparableAddr nodeInfo
                             in
                               nodeInfoStr `member` strPeerAddrs
                             where
                               toComparableAddr (NodeInfo h' p')
                                 = h' <> ":" <> show p' <> ":0"


--------------------------------------------------------------------------------
-- Main function
main :: IO ()
main = do
  -- Get the flags arguments and parse the config file
  argv         <- parseFlags <$> getArgs
  parsedConfig <- Y.decode <$> B.readFile "config.yaml" :: IO (Maybe Config)

  -- If everything went ok, run the program
  case argv of
    Left errs            -> error $ show errs
    Right (h, p, flags') ->
      if not (isIPv4address h || isIPv6address h)
         then error "Invalid address supplied"
         else case parsedConfig of
            Nothing     -> error "Config file invalid"
            Just config -> runProg h p flags' config


--------------------------------------------------------------------------------
-- Main function to run the program, takes a host:port string argument, a list
-- of flags (one of which is mandatory - --with-seed), and a config value
runProg :: String -> String -> [Flag] -> Config -> IO ()
runProg h p flags' config = do
  let sendTime = getSendTime flags'
  let waitTime = getWaitTime flags'
  let rndSeed  = fromMaybe (error "No seed specified!") (getSeed flags')

  -- With the above optional values, create the config value for our program
  let pConfig = config { sendPeriod  = fromMaybe (sendPeriod config) sendTime
                       , gracePeriod = fromMaybe (gracePeriod config) waitTime
                       }

  -- This log is important because I'm forcing the evalutaion of this
  -- value and therefore checking for the flag to exist before
  -- we send this value on a separate thread, where it can fail and make
  -- catching it a bit trickier. I should use a different library for
  -- parsing flags.
  doLog h p $ "Using random generator seed: " <> show rndSeed

  -- Start the TCP backend
  backend <- initializeBackend h p initRemoteTable
  -- Make ourselves discoverable
  node    <- newLocalNode backend
  -- Wait for all peers to be online
  _ <- runReaderT (waitForPeers node h p backend) pConfig
  -- Start the sender process and register it
  sender'   <- forkProcess node $
                  runReaderT (sender rndSeed h p node backend) pConfig
  runProcess node $ register "send-server" sender'
  -- Initialize a mutable variable with an empty list of `ProcMessage`
  messages  <- emptyMessageList
  -- Start the listener process and register it
  listener' <- forkProcess node $
                  runReaderT (listener messages) pConfig
  runProcess node $ register "receive-server" listener'

  -- Wait for the program to end as per the config file
  threadDelay $ gracePeriod config

  -- Read the mutable variable again (written previously by the `listener`)
  -- and output the result as per the document specification
  messages' <- liftIO $ readMVar messages
  print (length messages', getMessagesProducts messages')

  exitSuccess
  where
    getSendTime :: [Flag] -> Maybe Int
    getSendTime []            = Nothing
    getSendTime (SendFor i:_) = Just (1000000 * read i :: Int)
    getSendTime (_:xs)        = getSendTime xs

    getWaitTime :: [Flag] -> Maybe Int
    getWaitTime []            = Nothing
    getWaitTime (WaitFor i:_) = Just (1000000 * read i :: Int)
    getWaitTime (_:xs)        = getWaitTime xs

    getSeed :: [Flag] -> Maybe Int
    getSeed []                = Nothing
    getSeed (WithSeed i:_)    = Just (read i :: Int)
    getSeed (_:xs)            = getSeed xs
